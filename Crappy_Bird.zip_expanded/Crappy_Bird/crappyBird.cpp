/*
 * Crappy Bird
 * 
 * Author: Jeffrey Forte
 * 
 * 5/8/2020
 *
 */


#include <iostream>
#include <cstdlib>
#include <windows.h>
//#include <conio.h>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <stdio.h>
#include <termios.h>



using namespace std;

ifstream inp; //data file reading and writing operators
ofstream outp;


char c[30][21]; //variable for storing pixels
int n[30][21];  //variable for checking
int highscore; 
int contr,tuk=0,score=0,t=0,bt=0,birdx=0,birdy=0; //variables for maths calculations
bool err; //boolean for error detection


void game(); //various functions
void screen();
void pipes();
void bird();
bool gameover();
void help();
void menu();
void endgame();
void credits();

int main () {
    srand(time(0)); //seeding random number generator.
    
    inp.open("/Program Files/Flappy/Bird/options.txt"); // opening high score file
    if(inp.is_open()) //if file opens successfully, it reads high score
        { 
        inp>>highscore;
        inp.close();
        err=false; //error will be false, because file opened successfully 
        }
    else 
    {
        highscore=0; //if file doesn't exist, high score is 0 and err will be true
        err=true;
    }
    
    int a=0,b;
    char sl; //selection variable
    while(1) //loop for repeating actions after each start
    {
        if (a==0) goto play;
        if (a>0) //if you play not the first time, it will ask if you would like to play
        {
            score=0;
            cout<<"Do you want to play again? [y/n]";
            cin>>sl;
            if (sl=='n') goto quit;
            else goto play;
        }  
    play:
    menu(); //calling menu function
    switch(sl) //menu selections
        {
        case '1':
        {
            game();  //if you choose play: game function calls
            break;
        }
        case '2': //other selections-other functions
        {
            help();
            goto play;
            break;
        }
        case '3':
        {
            credits();
            goto play;
            break;
        }
        case '4':
        {
            goto quit; //exits game
            break;
        }
        default:
        {
            goto play; 
            break;
        }
        
    }
    a++; //variable for checking how many games played
    
    }
    quit:
    {
        cout<<"I quit."; //stops game and closes application.
    }
    
    return 0;
}

void game() //game function
{
    int x,y;
    char s;
    for(y=0;y<21;y++) //setting screen
    {
        for(x=0;x<30;x++)
        {
            if(y<20)
            {
                c[x][y]=' ';
                n[x][y]=0;
            }
            if (y==20)
            {
                c[x][y]='-';
                n[x][y]=2;
            }
            
        }
    }
    
    c[10][10]='*';                      //these coordinates will be the bird (*)
    screen();                            //calls for function showing screen
    while(1)        //loop starts, actual gameplay begins
    {
        s='~';      //default control variable
        Sleep(0.2*1000);                        //sets how fast everything moves
        t++;                                    //variable for time
        if(getche()==true)                             //if key is pressed, certain operation happen
        {
            s=getche();                      //gets what key is pressed
            if(s!='~') tuk=1;            //if not default, tuk=1 making bird go up
        }
        for(x=0,x<30;x++)                   //just setting ground
        {
            c[x][20]='-';
            n[x][20]=2;
        }
        
        bird();             //calls bird move function
        checkscore();        //checks score
        if(gameover()==true) goto gameEnd;  //checks if bird hits pipes, if yes, game ends
        pipes();                //spawns and moves pipes
        if(score>highscore) highscore=score;
        screen();               //calls screen function to show everything
        
        if(tuk>0) tuk++; //if key is pressed, bird flies up 2 times.
        if(tuk==3) tuk=0; //bird then falls
        
    }
    gameEnd;    //ends game
    {
        if(score>highscore) highscore=score;
        if(err==false) //if high score file exists, it writes new high score
        {
            outp.open("/Program Files/FlappyBird/opetions.txt");
            outp<<highscore;
            outp.close();
        }
    screen();      //shows ending screen, returns to int main
    endgame();
    return;
    }
}

void screen() //func for showing screen
{
    int x,y;
    system("cls");
    for(y=0;y<21;y++)
    {
        for(x=0;x<30;x++)
        {
            if(x<29) cout<<c[x][y];
            if(x==29) cout<<c[x][y]<<endl;
    }
 }
    cout<<""<<endl;
    cout<<"Your Score: "<<score;
    
}
    void pipes() //pipe movement and spawn function
    {
        int x,y,r;
        if(t==10) //if time is 10 or loop has passed 10x, new pipe spawns
        {
            r=(rand()%11)+5; //generates random num = pipe center
            for(y=0;y<20;y++) //only need y 
            {
                c[29][y]= "|"; //sets pipe
                n[29][y=2]; //n will be 2 checking if bird hits it
            }
            
            c[29][r-1]=' '; //sets hole
            c[29][r]= ' ';
            c[29][r+1]=' ';
            n[29][r-1]=0;
            n[29][r]=0;
            n[29][r+1]=0;
            t=0;
            goto mv; //move pipes
           }
    
        else goto mv;
        mv:                //pipe moving
        {
            for(y=0;y<20;y++) //loops for generating coordinates
            {
                for(x=0;x<30;x++)
                {
                    if c[x][y]=='|'; //all pipes will move left by 1
                        {
                        if(x>0)
                        {
                            c[x-1][y]='|';
                            n[x-1][y]=2;
                            c[x][y]=' ';
                            n[x][y]=0;
                        }
                        if(x==0) //if screen ends x=0 pipe will dissapear
                        {
                            c[x][y]' ';
                            n[x][y]=0;
                        }
                        
                    } 
                }
            }
     
        }
    
}
    void bird() //bird move function
    {
        int x,y;
        if(tuk>0) //if key pressed, bird goes up
        {
            bt=0;
            for(y=0;y<20;y++) //loop for finding biird location
            {
                for(x=0;x<20;x++)
                {
                    if(c[x][y]=='*')
                    {
                        if(y>0)
                        {
                            c[x][y-1]='*'; //bird moves up by 1
                            c[x][y]=' ';
                            birdx=x;        //sets bird xcoord
                            birdy=y-1;       //sets bird ycoord
                            return;
                            }
                        }
                    }
                }
            }
            else //if no key pressed, bird falls
            {
               bt++;
            for(y=0;y<20;y++) //loop for finding biird location
            {
                for(x=0;x<20;x++)
                {
                    if(c[x][y]=='*')
                    {
                        if(y<20) //if bird is not on ground
                        {
                            if(bt<3) //if bird time is lower than 3, it fals 1 pixel
                            {
                        
                            c[x][y+1]='*'; //bird moves up by 1
                            c[x][y]=' ';
                            birdx=x;        //sets bird xcoord
                            birdy=y+1;       //sets bird ycoord
                            return;
            }
                            else if(bt>2 && bt<5) //more time passes, bird accelerates
                            {
                                c[x][y+2]='*';
                                c[x][y]=' ';
                                birdx=x;
                                birdy=y+2;
                                return;
                            }
                            else if(bt> 4)  //more time, more acceleration
                            {
                                c[x][y+3]='*';
                                c[x][y]=' ';
                                birdx=x;
                                birdy=y+3;
                                return;
                            }    
        }
    else
    {
                        return; //if bird is already on ground func checks for game over
                            }
                    }
                }
            }
        }
    }
    
void checkscore() //checks if bird gains points
{
    int y;
    for(y;y<20;y++)
    {
        if(c[birdx][y]=='|') //if birdx is equal to the pipe coord, get 1 point
       {
        score++;
        return;
        }
    }
}

bool gameover() //checks if bird has hit something
{
    int x,y,f=0;
    if(birdy>19) //checks if bird hits ground
    {
        c[birdx][19]='*';
        c[birdx][20]='-';
        f=1;                //f=1 means func will return 
        goto quit;
    }
    else                //checks if bird has hit pipes
    {
    if(n[birdx][birdy]>0 && (c[birdx][birdy]== '|' || c[birdx][birdy]=='*'))
    {
        c[birdx][birdy]='|';
        c[birdx][19]='*';
        f=1;
        goto quit;
    }
  }
quit:
            if(f==1) return true;
            else return false;
    
}    

void endgame() //for funzies
{
    screen();  //if game ends
    cout<<""<<endl<<endl;
    cout<<" ------------------------------------------------------------------------- "<<endl;
    cout<<"|    *****      *     *       * ******       ****  *       ****** ****    |"<<endl;
    cout<<"|   *          * *    * *   * * *           *    *  *     * *     *   *   |"<<endl;
    cout<<"|   *  ****   *   *   *  * *  * *****       *    *   *   *  ****  ****    |"<<endl;
    cout<<"|   *  *  *  *******  *   *   * *           *    *    * *   *     * *     |"<<endl;
    cout<<"|    *****  *       * *       * ******       ****      *    ***** *   *   |"<<endl;
    cout<<" ------------------------------------------------------------------------- "<<endl;
    cout<<""<<endl<<endl;
    cout<<"                        Y O U R   S C O R E : "<<score<<endl<<endl;
    cout<<"                        H I G H   S C O R E : "<<highscore<<endl;
    cout<<""<<endl<<endl;
}

void menu()
{
    system("cls");
    cout<<""<<endl;
    cout<<" --------------------------------------------------------  "<<endl;
    cout<<"|                                                        | "<<endl;
    cout<<"|   **** *    **** **** **** *   *    ***  * ***  ***    | "<<endl;
    cout<<"|   *    *    *  * *  * *  * *   *    *  * * *  * *  *   | "<<endl;
    cout<<"|   ***  *    **** **** **** *****    ***  * ***  *  *   | "<<endl;
    cout<<"|   *    *    *  * *    *      *      *  * * *  * *  *   | "<<endl;
    cout<<"|   *    **** *  * *    *      *      ***  * *  * ***    | "<<endl;
    cout<<"|                                                  v 1.0 | "<<endl;
    cout<<" --------------------------------------------------------  "<<endl;
    cout<<""<<endl<<endl;
    cout<<"                  High Score:  "<<highscore<<endl<<endl;
    cout<<""<<endl<<endl;
    cout<<"                     M E N U:    "<<endl<<endl;
    cout<<"                  1: Start Game  "<<endl<<endl;
    cout<<"                  2: Help        "<<endl<<endl;
    cout<<"                  3: Credits     "<<endl<<endl;
    cout<<"                  4: Exit        "<<endl<<endl;
    
}

void credits()
{
    char sel;
    system("cls");
    while(true)
    {
        cout<<""<<endl<<endl;
    cout<<"               Lead programmer: Jeffrey Forte "<<endl<<endl;
    cout<<"               Designer: Jeffrey Forte "<<endl<<endl;
    cout<<"               Testers: Jeffrey Forte "<<endl<<endl;
    cout<<"               Special thanks to: hakeris1010 ,Dong Nguyen (original)"<<endl<<endl;
    cout<<"               Version: 1.0 "<<endl<<endl<<endl;
    cout<<"Go back? [y/n] ";
    cin>>sel;
    if(sel=='y') return;
    else system("cls");
    }
}

void help()
{
    char sel;
    system("cls");
    while(true)
    {
        cout<<""<<endl<<endl;
        cout<<"                     Controls: Press any key to fly up.          "<<endl<<endl;
        cout<<"             Goal: Fly through the holes between pipes.          "<<endl;
        cout<<"             When you pass through the hole, you get 1 point.    "<<endl;
        cout<<"                 Try to pass as many as you can.                 "<<endl;
        cout<<"             Be careful to not hit the pipes or the GROUND!      "<<endl<<endl;
        cout<<"                             GET SOME!                           "<<endl<<endl<<endl;
        cout<<"Go Back? [y/n]   ";
        cin>>sel;
        if(sel=='y') return;
        else system("cls");
    }  
}